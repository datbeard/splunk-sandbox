import sys
import json
import urllib2
import datetime

def send_discord_message(settings):


    #load Splunk Json, extract raw alert, source and time
    body = json.dumps(payload)
    resp = json.loads(body)


    # declare all the fields for the message
    time = resp['result']['_time']
    timezone = resp['result']['timezone']
    trait = resp['result']['trait']
    discord_user = resp['result']['discord']
    division = resp['result']['division']
    personality = resp['result']['personality']
    dedication = resp['result']['dedication']
    endgame = resp['result']['endgame']
    country = resp['result']['Country']
    region = resp['result']['Region']
    ip = resp['result']['src_ip']


    # converttime to human readable format
    human = datetime.datetime.fromtimestamp(
        float(time)
    ).strftime('%Y-%m-%d %H:%M:%S')

    #Grab Configuration Settings
    headers = {
        "User-Agent": 'Splunk',
        "Content-Type": "application/json"
    }
    url = config.get('url')

    #the actual payload that goes to discord
    discordload = {
        "embeds": [{

                 "title": "Application",
                 "color": 578568,
                 "fields": [
                     {
                         "name": "Discord",
                         "value": discord_user
                     },
                     {
                         "name": "Trait",
                         "value": trait
                     },
                     {
                         "name": "Division",
                         "value": division
                     },
                     {
                         "name": "Dedication",
                         "value": dedication
                     },
                     {
                         "name": "Personality",
                         "value": personality
                     },
                     {
                         "name": "Country",
                         "value": country
                     },
                     {
                         "name": "Region",
                         "value": region
                     },
                     {
                         "name": "Timezone",
                         "value": timezone
                     },
                     {
                         "name": "Submitted Time",
                         "value": human
                     },
                     {
                         "name": "IP Address",
                         "value": ip
                     }
                 ]
        }]
    }
    
    #Encodes the Discord Payload to be used with urllib
    body = json.dumps(discordload)

    print >> sys.stderr, 'DEBUG Calling url="%s" with body=%s' % (url, body)
    req = urllib2.Request(url, body, headers)
    try:
        res = urllib2.urlopen(req)
        body = res.read()
        print >> sys.stderr, "INFO discord API responded with HTTP status=%d" % res.code
        print >> sys.stderr, "DEBUG discord API response: %s" % json.dumps(body)
        return 200 <= res.code < 300
    except urllib2.HTTPError, e:
        print >> sys.stderr, "ERROR Error sending message: %s" % e
        print >> sys.stderr, json.dumps(body)
        return False


if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == "--execute":
        payload = json.loads(sys.stdin.read())
        config = payload.get('configuration')
        if not send_discord_message(config):
            print >> sys.stderr, "FATAL Sending the discord message failed"