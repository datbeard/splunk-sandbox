up:
	@echo "BRINGIN' ER UP CAP!!"
	@docker-compose up

stop:
	@echo "Brinin' er to a stop, Cap"
	@docker-compose stop

start:
	@echo "Startin the engines, Cap"
	@docker-compose start

down:
	@echo "Bringin' er down, Cap"
	@docker-compose down
	@echo "Deleting log folder"
	@rm -rf ./opt/splunkforwarder/var/log

restart:
	@echo "Restartin' da Splunk indexer, Cap"
	@docker exec -ti splunk-sandbox_indexer_1 sh -c "sudo /opt/splunk/bin/splunk restart"

docker-clean:
	@echo "LET'S CLEAN THIS THING UP!"
	@echo "Removing all non running containers"
	@docker rm `docker ps -q -f status=exited`
	@echo "Deleting all untagged/dangling (<none>) images"
	@docker rmi `docker images -q -f dangling=true`