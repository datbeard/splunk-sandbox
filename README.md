#  Splunk Sandbox Docker Environment

A very simple sandbox environment with an indexer and forwarder for getting familiar with Splunk UI and how log ingestion works.

#### Prerequisites:
- Admin Access to your machine
- Docker

#### Optional Tools(Mac):
- Apple Developer Tools for Makefile 
    - Installation: `xcode-select --install`
***
## Bringing up the Enviroment

#### MacOS

In the terminal within the project directory run `make up` to bring up the environment, alternatively just run the `docker-compose up` command.

#### Windows

In command prompt/terminal run `docker-compose up` within the project directory.
***
## Access Splunk

Once the `docker-compose` is done you should be able to simply navigate to `localhost:8000` in your internet browser to accces the splunk login page from use username:`admin` password:`password` for login.

## Configurations

In order to have the indexer and forwarder communicate a few splunk configuration files are copied over to the containers. 

The files can be found in the `configs/` directory
```
configs/
├── forwarder
│   ├── inputs.conf
│   └── outputs.conf
└── indexer
    └── inputs.conf
```

### Forwarder
##### inputs.conf

```
[monitor:///gologs/*.log]                                    
host = uf.gs.splunk.io
disabled = 0                                                                                           
sourcetype = generic_single_line                                                                                 
index = main

[monitor:///logs/tutorialdata/mailsv/secure.log]                            
host = uf.gs.splunk.io
disabled = 0                                                                                           
sourcetype = linux_secure                                                                              
index = main

[monitor:///logs/tutorialdata/www1/access.log]                            
host = uf.gs.splunk.io
disabled = 0                                                                                           
sourcetype = access_common                                                                              
index = main
                                 
[splunktcp://9997]                                          
connection_host = none
```

In `inputs.conf` we set monitors on specific log files within our project directory, this can be changed if you would like to ingest your own logs, just add them to the `data_ingest/` directory and create a corresponding monitor above.

##### outputs.conf

```
[tcpout]                                                    
defaultGroup = splunk                                       
                                                            
[tcpout:splunk]                                             
server = so1.gs.splunk.io:9997
                                                            
[indexAndForward]                                           
index = false
```
In `outputs.conf` we set the group name which we are just setting to splunk and the indexer hostname and port we are communicating on, no need to change this unless you are adding and changing indexers or other configurations.

### Indexer
###### inputs.conf
```
[default]                      
host = gs.splunk.io                                                        

[splunktcp://9997]                                                                                                                               
disabled = 0
```
Nothing special for the `inputs.conf` for the indexer, we just set the hostname and port we are accepting forwarding on